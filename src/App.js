import React, {Component} from 'react';
import Pages from './pages';
import {Link} from 'react-router-dom';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCommentDots} from '@fortawesome/free-solid-svg-icons'
class App extends Component {
    render(){
        return (
            <React.Fragment>
                <section>
                    <div className = "container">
                        <ul className = "top-bar d-flex justify-content-end mt-5">
                            <li className="border-right align-middle">
                                <Link to="#">Sign In</Link>
                            </li>
                            <li>
                                <Link to="contact-us" className="contact-us btn btn-primary">
                                    <i className="far fa-comment-dots"></i>
                                    <FontAwesomeIcon icon={faCommentDots}/> Contact Us
                                    </Link>
                            </li>
                        </ul>
                    </div>
                </section>
                <nav className="navbar navbar-expand-lg ">
                    <div className="container">
                    <div className="collapse navbar-collapse justify-content-center ">
                    <ul className="navbar-nav ">
                        <li className="nav-item"><Link to="#"  className="nav-link">HOME</Link></li>
                        <li className="nav-item"><Link to="#"  className="nav-link">Start Learn</Link></li>
                        <li className="nav-item"><Link to="#"  className="nav-link">Career Path</Link></li>
                        <li className="nav-item"><Link to="#"  className="nav-link">Books</Link></li>
                        <li className="nav-item"><Link to="#"  className="nav-link"  >Videos</Link></li>

                    </ul>
                    </div>
                    </div>
                </nav>
            <div className="container">
                <Pages.BlogPage/> 
            </div>
            </React.Fragment>
        );
    }
}

export default App;